Lookup a definition
Narrative:
In order to talk better
As an English student
I want to look up word definitions

Meta:
@feature Wiki

Scenario: Looking up the definition of 'apple' - wiki_test.story
Given Login my system with demo and demo
Given the user is on the Wikionary home page
When the user looks up the definition of the word 'apple'
Then they should see the definition 'A common, round fruit produced by the tree Malus domestica, cultivated in temperate climates.'


Scenario: Looking up the definition of 'apple' 222 - wiki_test.story
Given the user is on the Wikionary home page
When the user looks up the definition of the word 'apple'
Then they should see the definition 'A common, round fruit produced by the tree Malus domestica, cultivated in temperate climates.'
