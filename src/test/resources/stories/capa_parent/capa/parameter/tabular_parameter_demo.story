Login local page
Narrative:
In order to talk better 2

Meta:
@feature TabularParameters
Scenario: Tabular_parameter Scenario 1 - tabular_parameter.story
Given the traders ranks are: 
|name|rank|
|Larry|Stooge 3|
|Moe|Stooge 1|
|Curly|Stooge 2|
When Traders are subset to ".*y" by name
Then the traders returned are:
|name|rank|
|Larry|Stooge 3|
|Curly|Stooge 2|