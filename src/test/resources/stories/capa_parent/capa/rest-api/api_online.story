API ONLINE Test
Narrative:
Rest assured


Meta:
@feature Api
@metatest Trung
@issues FH-17

Scenario: API online scenario 1 - api_online.story
Given Prepare service
When Get user information with id is 1
When Get user class converter with id is 1
When Get user class annotation converter with id is 1
Then username of user is Bret
And company.name of user is Romaguera-Crona
And Datetime test: 02/14/2015,company.name,Romaguera-Crona
Having company.name of user is Romaguera-Crona

