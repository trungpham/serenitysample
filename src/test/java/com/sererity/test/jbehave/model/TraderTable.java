package com.sererity.test.jbehave.model;

public class TraderTable {
	private String name;
	private String rank;
	public TraderTable(String name, String rank) {
		super();
		this.name = name;
		this.rank = rank;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
}
