package com.sererity.test.jbehave.converters;

import java.lang.reflect.Type;

import org.jbehave.core.steps.ParameterConverters.ParameterConverter;

public class PersonConverter implements ParameterConverter {

//    public static final DateFormat DEFAULT_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

//    private final DateFormat dateFormat;

    public PersonConverter() {
//        this(DEFAULT_FORMAT);
    }

//    public DateConverter(DateFormat dateFormat) {
//        this.dateFormat = dateFormat;
//    }

    public boolean accept(Type type) {
        if (type instanceof Class<?>) {
            return Person.class.isAssignableFrom((Class<?>) type);
        }
        return false;
    }

    public Object convertValue(String value, Type type) {
    	Person ps = new Person();
    	ps.setName(value);
    	ps.setEmail("email" + value + "@gmail.com");
    	ps.setId(Integer.valueOf(value));
        return ps;
    }

}