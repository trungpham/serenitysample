package com.sererity.test.jbehave.retrofit.service;

import java.util.List;

import com.sererity.test.jbehave.retrofit.model.UserApi;

import retrofit.http.GET;
import retrofit.http.Path;

public interface UserService {
	
	@GET("/users/{user_id}")
	public UserApi get(@Path("user_id") int user);
}
