package com.sererity.test.jbehave.retrofit.model;

import gherkin.deps.com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import org.parceler.Parcel;


public class UserApi implements Serializable{
	
	private int id;
	
	private String name;
	
	private String email;
	
	private String username;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
