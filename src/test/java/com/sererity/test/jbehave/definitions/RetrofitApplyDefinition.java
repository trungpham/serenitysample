package com.sererity.test.jbehave.definitions;

import static org.assertj.core.api.Assertions.assertThat;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

import com.serenity.test.custom.core.DefinitionCore;
import com.sererity.test.jbehave.retrofit.model.UserApi;
import com.sererity.test.jbehave.retrofit.service.UserService;
import com.squareup.okhttp.OkHttpClient;

public class RetrofitApplyDefinition extends DefinitionCore{
	private static final Logger log = LoggerFactory.getLogger(RetrofitApplyDefinition.class);
    
	private RestAdapter restAdapter;
	private UserService service;
	private UserApi userApi;
	
    @Given("Prepare retrofit service")
    public void prepareService() {
    	restAdapter = new RestAdapter.Builder()
        .setEndpoint("http://jsonplaceholder.typicode.com/")
        .build();
    	OkHttpClient okHttpClient = new OkHttpClient();
    	new RestAdapter.Builder().setExecutors(null, null).setClient(new OkClient(okHttpClient)).;
    	service = restAdapter.create(UserService.class);
    }
    
    @When("Get user by retrofit service with id is $user_id")
    public void userLoginWithUserNameAndPass(int user_id) {
    	userApi = service.get(user_id);
    }
    
    @Then("Retrofit will return $userProperty is $userPropertyValue")
    public void verifyLoginSuccess(String userProperty, String userPropertyValue) {
    	assertThat(userApi.getUsername()).containsIgnoringCase(userPropertyValue);
    }
}
