package com.sererity.test.jbehave.definitions;

import net.thucydides.core.annotations.Steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.serenity.test.custom.core.DefinitionCore;
import com.sererity.test.steps.DjangoAdminSteps;

public class DjangoAdminDefinition extends DefinitionCore{
	private static final Logger log = LoggerFactory.getLogger(DjangoAdminDefinition.class);
    @Steps
    DjangoAdminSteps localStep;

    @Given("User open login page")
    public void givenTheUserOpenLoginPage() {
        localStep.is_the_home_page();
    }

    @When("User login with $username and $password")
    public void userLoginWithUserNameAndPass(String username, String password) {
        localStep.login(username, password);
    }
    
    @Then("login success")
    public void verifyLoginSuccess() {
    	localStep.verify_login_success();
    }

}

