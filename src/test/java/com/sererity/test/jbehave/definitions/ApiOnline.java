package com.sererity.test.jbehave.definitions;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;

import java.io.IOException;
import java.util.Date;

import net.serenitybdd.plugins.jira.guice.Injectors;
import net.thucydides.core.requirements.FileSystemRequirementsTagProvider;
import net.thucydides.core.statistics.service.TagProvider;
import net.thucydides.core.statistics.service.TagProviderService;
import net.thucydides.core.util.EnvironmentVariables;

import org.jbehave.core.annotations.AsParameterConverter;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;
import com.jayway.restassured.response.Response;
import com.serenity.test.custom.config.DatabaseConfig;
import com.serenity.test.custom.core.DefinitionCore;
import com.sererity.test.jbehave.converters.Person;
import com.sererity.test.jbehave.converters.PersonAnnotation;

@ContextConfiguration("/database-config.xml")
//@ContextConfiguration(classes=DatabaseConfigurationAnnotation.class, loader=AnnotationConfigContextLoader.class)
public class ApiOnline extends DefinitionCore{
	private static final Logger log = LoggerFactory.getLogger(ApiOnline.class);
    
//	private static String ROOT_URL = "http://jsonplaceholder.typicode.com/";
	private static String USER_SERVICE_NAME = "/users";
	Response response;
	
	@AsParameterConverter
    public PersonAnnotation createPersonAnnotation(String userId){
		PersonAnnotation a = new PersonAnnotation();
		a.setId(Integer.valueOf(userId));
		a.setName(userId);
		a.setEmail("emailnew" + userId + "@gmail.com");
        return a;
    }
	
	Gson gson = new GsonBuilder()
    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
    .registerTypeAdapter(Date.class, new DateTypeAdapter())
    .create();

	
	@Autowired
	private DatabaseConfig databaseConfig;
	
	public void logDatabaseConfigInfo(){
		log.info("-------------------start logDatabaseConfigInfo---------------");
		log.info("getConnectionString:"+databaseConfig.getConnectionString());
		log.info("getConnection:"+databaseConfig.getConnection());
		log.info("shutDown:"+databaseConfig.shutDown());
		log.info("extraInfo:"+databaseConfig.getExtraInfoInterface());
		log.info("env [thucydides.public.url]:"+databaseConfig.getEnvironmentVariables().getProperty("thucydides.public.url"));
		log.info("-------------------end logDatabaseConfigInfo---------------");
	}
	
    @Given("Prepare service")
    public void prepareService(@Named("metatest") String metatest) throws IOException {
    	log.info("Get meta test:" + metatest);
    	logDatabaseConfigInfo();
//    	FileSystemRequirementsTagProvider
    	EnvironmentVariables env = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
    	FileSystemRequirementsTagProvider fileSysTagProvider = Injectors.getInjector().getProvider(FileSystemRequirementsTagProvider.class).get();
    	log.info("Requirement DIR:" + fileSysTagProvider.getResourceDirectory(env));
    	log.info("Root directory paths:" + fileSysTagProvider.getRootDirectoryPaths());
    	log.info("Default Root directory from env:" + FileSystemRequirementsTagProvider.defaultRootDirectoryPathFrom(env));
    	
    	TagProviderService tagProviderServ = Injectors.getInjector().getProvider(TagProviderService.class).get();
    	for (TagProvider tagProv : tagProviderServ.getTagProviders()) {
			log.info("Current tag provider:" + tagProv.toString());
		}
    	
    	
//    	JIRAConfiguration jiraConfiguration = Injectors.getInjector().getInstance(JIRAConfiguration.class);
    	log.info("--------------JIRA CONFIG--------------");
//    	log.info(jiraConfiguration.getJiraUser());
        log.info("Prepare service done");
    }
    
    @When("Get user information with id is $user_id")
    public void userLoginWithUserNameAndPass(int user_id) {
    	response = given().get(USER_SERVICE_NAME + "/" + user_id);
    	response.then().body("id", equalTo(user_id));
    }
    
    @When("Get user class converter with id is $user_id")
    public void userClassConverter(Person person) {
    	log.info("Email Person:" + person.getEmail());
    }
    
    @When("Get user class annotation converter with id is $user_id")
    public void userClassConverterAnnotation(PersonAnnotation personAnnotation) {
    	log.info("Email Person:" + personAnnotation.getEmail());
    }
    
    @Then("$keyProp of user is $valueProp")
    public void verifyLoginSuccess(String keyProp, String valueProp) {
    	response.then().body(keyProp, equalToIgnoringCase(valueProp));
//    	response.then().body("username", equalToIgnoringCase(userName)).body("company.name.title", equalToIgnoringCase(companyName));
    }
    
    @Then("Datetime test: $datetime,$keyProp,$valueProp")
    public void verifyLoginSuccess(Date datetime, String keyProp, String valueProp) {
    	log.info("Date time:" + datetime);
    	response.then().body(keyProp, equalToIgnoringCase(valueProp));
//    	response.then().body("username", equalToIgnoringCase(userName)).body("company.name.title", equalToIgnoringCase(companyName));
    }
    
}
