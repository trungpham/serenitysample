package com.sererity.test.jbehave.definitions;

import net.thucydides.core.annotations.Steps;

import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.serenity.test.custom.core.DefinitionCore;
import com.sererity.test.steps.WikiSteps;


public class WikiDefinition extends DefinitionCore{

    @Steps
    WikiSteps wikiSteps;
    
    private static final Logger log = LoggerFactory.getLogger(WikiDefinition.class);
    
    @Given("the user is on the Wikionary home page")
    public void givenTheUserIsOnTheWikionaryHomePage() {
        wikiSteps.is_the_home_page();
    }

    @When("the user looks up the definition of the word '$word'")
    public void whenTheUserLooksUpTheDefinitionOf(String word) {
        wikiSteps.looks_for(word);
    }

    @Then("they should see the definition '$definition'")
    public void thenTheyShouldSeeADefinitionContainingTheWords(String definition) {
        wikiSteps.should_see_definition(definition);
    }

}
