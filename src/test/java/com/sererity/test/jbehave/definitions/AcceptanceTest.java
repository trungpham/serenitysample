package com.sererity.test.jbehave.definitions;

import java.text.SimpleDateFormat;

import net.serenitybdd.jbehave.SerenityStories;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.embedder.EmbedderControls;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.ParameterControls;
import org.jbehave.core.steps.ParameterConverters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.jayway.restassured.RestAssured;
import com.serenity.test.custom.config.DatabaseConfig;
import com.serenity.test.custom.config.DatabaseConfigurationAnnotation;
import com.serenity.test.custom.config.MySqlDatabaseConfig;
import com.sererity.test.jbehave.converters.PersonConverter;

public class AcceptanceTest extends SerenityStories {
	
	private static final Logger log = LoggerFactory
			.getLogger(AcceptanceTest.class);
	
	public AcceptanceTest() {
		// EnvironmentVariables env =
		// getSystemConfiguration().getEnvironmentVariables();
		// boolean useSingleSessionWindow =
		// env.getPropertyAsBoolean("use.single.session.window", false);
		// reportBuilder.withFormats(Format.CONSOLE, Format.STATS);
		// config.useStoryReporterBuilder(reportBuilder);

		// log.info("\n\n--------------------------------------------");
		// log.debug("DEBUG");
		// log.info("INFO: {} {}", 0, 1);
		// log.error("ERROR");
		// log.info("---------------------------------------------------\n\n");

		// if (useSingleSessionWindow){
		// runSerenity().inASingleSession();
		// }
		restAssuredConfig();
	}
	

	public void restAssuredConfig() {
		RestAssured.baseURI = "http://jsonplaceholder.typicode.com";
//		RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
//		 RestAssured.requestSpecification = new RequestSpecBuilder().addParameter("parameter1", "value1").build();
	}
	
	

	@Override
	public Embedder configuredEmbedder() {
		// TODO Auto-generated method stub
		
		log.info("-----------------------configuredEmbedder NOT WORKING-------------------------------");
		log.info("-----------------------configuredEmbedder NOT WORKING-------------------------------");
		Embedder embedder = super.configuredEmbedder();
		EmbedderControls embedderControls = embedder.embedderControls();
//		embedderControls.doIgnoreFailureInStories(true).doIgnoreFailureInView(true).doVerboseFailures(false);
		log.info("-----------------------"+embedderControls.ignoreFailureInStories()+"-------------------------------");
		log.info("-----------------------"+embedderControls.ignoreFailureInStories()+"-------------------------------");
		return embedder;
	}

	@Override
	public org.jbehave.core.configuration.Configuration configuration() {
		// TODO Auto-generated method stub
		Configuration config = super.configuration();
		StoryReporterBuilder reportBuilder = config.storyReporterBuilder();
		// reportBuilder.withFormats(Format.CONSOLE, Format.STATS);
		// config.useStoryReporterBuilder(reportBuilder);
		ParameterControls controls = config.parameterControls();
		// log.info("---------_CONTROLS ------------");
		// log.info(controls.toString());
		//
//		 log.info("------------KEYWORDS---------------");
//		 log.info(config.keywords().toString());
		// log.info("------------paranamer---------------");
		// log.info(config.paranamer().toString());
		// log.info("------------parameterConverters---------------");
		ParameterConverters converters = config.parameterConverters();
		converters.addConverters(
				new org.jbehave.core.steps.ParameterConverters.DateConverter(
						new SimpleDateFormat("MM/dd/yyyy")));
		converters.addConverters(new PersonConverter());
		config.useParameterConverters(converters);
//		config.usePendingStepStrategy(new FailingUponPendingStepCustom());
//		config.useFailureStrategy(new SilentlyAbsorbingFailure());
		return config;
	}
	
	

	@Override
	public InjectableStepsFactory stepsFactory() {
		// return new InstanceStepsFactory(configuration(), new TraderSteps(new
		// TradingService()), new AndSteps(),
		//                 new CalendarSteps(), new PriorityMatchingSteps(), new
		// SandpitSteps(), new SearchSteps(),
		//                 new BeforeAfterSteps());
		InjectableStepsFactory stepFactory = super.stepsFactory();
		// log.info("------------STEP FACTORY---------------");
		// for (int i = 0; i < stepFactory.createCandidateSteps().size(); i++) {
		// log.info(stepFactory.createCandidateSteps().get(i).listCandidates()
		// .get(0).toString());
		// }
		return stepFactory;
	}

}
