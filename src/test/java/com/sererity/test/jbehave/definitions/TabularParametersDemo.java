package com.sererity.test.jbehave.definitions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.model.ExamplesTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.serenity.test.custom.core.DefinitionCore;
import com.sererity.test.jbehave.model.TraderTable;

public class TabularParametersDemo extends DefinitionCore{
	private static final Logger log = LoggerFactory.getLogger(TabularParametersDemo.class);
	private ExamplesTable ranksTable;
	private List<TraderTable> traders;
	
	@Given("the traders ranks are: $ranksTable")
	public void theTraders(ExamplesTable ranksTable) {
	    this.ranksTable = ranksTable;
	    this.traders = toTraders(ranksTable);
	    log.info("traders:" + this.traders.get(0).getName());
	}
	 
	private List<TraderTable> toTraders(ExamplesTable table) {
	    List<TraderTable> traders = new ArrayList<TraderTable>();
	    for (Map<String,String> row : table.getRows()) {
	        String name = row.get("name");
	        String rank = row.get("rank");
	        traders.add(new TraderTable(name, rank));
	    }
	    return traders;
	}
}
