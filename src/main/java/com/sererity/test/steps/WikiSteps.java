package com.sererity.test.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

import org.jbehave.core.annotations.BeforeScenario;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import com.serenity.test.custom.core.DefinitionCore;
import com.sererity.test.pages.WikiPage;
public class WikiSteps extends ScenarioSteps {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(WikiSteps.class);
	private WikiPage wikiPage;
    
	@BeforeScenario
    public void beforeEachScenario() {
        log.info("----CALL beforeEachScenario1");
    }
	
    @Step
    public void enters(String keyword) {
        wikiPage.enter_keywords(keyword);
    }

    @Step
    public void starts_search() {
        wikiPage.lookup_terms();
    }

    @Step
    public void should_see_definition(String definition) {
        assertThat(wikiPage.getDefinitions(), hasItem(containsString(definition)));
    }

    @Step
    public void is_the_home_page() {
        wikiPage.open();
    }

    @Step
    public void looks_for(String term) {
        enters(term);
        starts_search();
    }
}