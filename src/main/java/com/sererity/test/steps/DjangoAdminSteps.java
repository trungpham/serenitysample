package com.sererity.test.steps;

import static org.assertj.core.api.Assertions.assertThat;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import com.sererity.test.pages.DjangoAdminPage;

public class DjangoAdminSteps extends ScenarioSteps {

    DjangoAdminPage myLocalPage;
    
    @Step
    public void is_the_home_page() {
        myLocalPage.open();
    }

    @Step
    public void login(String username, String password) {
    	myLocalPage.enter_username(username);
    	myLocalPage.enter_password(password);
    	myLocalPage.go_to_login_page();
    }
    	
    @Step
    public void verify_login_success() {
    	assertThat(myLocalPage.getTitle()).containsIgnoringCase("Site admin");
    }

    
    
}