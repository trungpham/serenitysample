package com.sererity.test.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("http://djangosuit.com/admin/")
public class DjangoAdminPage extends PageObjectCustom {

    @FindBy(name="username")
    private WebElementFacade usernameInp;

    @FindBy(name="password")
    private WebElementFacade passwordBtn;
    
    
    @FindBy(css=".btn.btn-info")
    private WebElementFacade loginBtn;
    
    
    public void enter_username(String username) {
        usernameInp.type(username);
    }

    public void enter_password(String password) {
    	passwordBtn.type(password);
    }
    
    
    public void go_to_login_page(){
    	loginBtn.waitUntilClickable().click();
    }

}