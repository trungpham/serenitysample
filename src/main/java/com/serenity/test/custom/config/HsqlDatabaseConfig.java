package com.serenity.test.custom.config;

import net.serenitybdd.plugins.jira.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

public class HsqlDatabaseConfig implements DatabaseConfig{
	
	private String extraInfo;
	private EnvironmentVariables env;
	
	public HsqlDatabaseConfig(){
		this("Default extra info - HsqlDatabaseConfig");
	}
	
	public HsqlDatabaseConfig(String extraInfo) {
		this.extraInfo = extraInfo;
	}

	public String getExtraInfo() {
		return extraInfo;
	}

	public void setExtraInfo(String extraInfo) {
		this.extraInfo = extraInfo;
	}

	@Override
	public String getConnectionString() {
		return "HsqlDatabaseConfig-getConnectionString";
	}

	@Override
	public String getConnection() {
		return "HsqlDatabaseConfig-getConnection";
	}

	@Override
	public String shutDown() {
		return "HsqlDatabaseConfig-shutDown";
	}

	@Override
	public String getExtraInfoInterface() {
		return extraInfo;
	}

	@Override
	public EnvironmentVariables getEnvironmentVariables() {
		if (env == null){
			env = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
		}
		return env;
	}
}
