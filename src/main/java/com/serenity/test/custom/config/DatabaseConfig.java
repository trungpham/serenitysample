package com.serenity.test.custom.config;

import net.thucydides.core.util.EnvironmentVariables;

public interface DatabaseConfig {
	public String getConnectionString();
	public String getConnection();
	public String shutDown();
	public String getExtraInfoInterface();
	public EnvironmentVariables getEnvironmentVariables();
}
