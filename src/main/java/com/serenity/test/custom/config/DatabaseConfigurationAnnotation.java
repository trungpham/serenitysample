package com.serenity.test.custom.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatabaseConfigurationAnnotation {
	
	private static final Logger log = LoggerFactory
			.getLogger(DatabaseConfigurationAnnotation.class);
	
	@Bean
    public DatabaseConfig databaseConfig() {
		DatabaseConfig orderService = new MySqlDatabaseConfig();
//		DatabaseConfig orderService = new MySqlDatabaseConfig("Custom extra info");
        // set properties, etc.
		log.info("---------------------Database config in DatabaseConfiguration.java--------------------------");
        return orderService;
    }
}
