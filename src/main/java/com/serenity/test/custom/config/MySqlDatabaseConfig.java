package com.serenity.test.custom.config;

import com.google.inject.Inject;

import net.serenitybdd.plugins.jira.guice.Injectors;
import net.thucydides.core.requirements.FileSystemRequirementsTagProvider;
import net.thucydides.core.util.EnvironmentVariables;

public class MySqlDatabaseConfig implements DatabaseConfig{
	
	private String extraInfo;
	private EnvironmentVariables env;
	
	public MySqlDatabaseConfig(){
		this("Default extra info - MySqlDatabaseConfig");
	}
	
	public MySqlDatabaseConfig(String extraInfo) {
		this.extraInfo = extraInfo;
	}
	
	@Override
	public String getConnectionString() {
		return "MySqlDatabaseConfig-getConnectionString";
	}

	public String getExtraInfo() {
		return extraInfo;
	}

	public void setExtraInfo(String extraInfo) {
		this.extraInfo = extraInfo;
	}
	
	
	@Override
	public String getConnection() {
		return "MySqlDatabaseConfig-getConnection";
	}

	@Override
	public String shutDown() {
		return "MySqlDatabaseConfig-shutDown";
	}

	@Override
	public String getExtraInfoInterface() {
		return extraInfo;
	}

	@Override
	public EnvironmentVariables getEnvironmentVariables() {
		if (env == null){
			env = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
		}
		return env;
	}

}
