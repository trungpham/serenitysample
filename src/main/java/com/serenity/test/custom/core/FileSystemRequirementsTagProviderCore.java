package com.serenity.test.custom.core;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;

import net.thucydides.core.ThucydidesSystemProperty;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.requirements.FileSystemRequirementsTagProvider;
import net.thucydides.core.requirements.model.NarrativeReader;
import net.thucydides.core.requirements.model.Requirement;
import net.thucydides.core.util.EnvironmentVariables;

public class FileSystemRequirementsTagProviderCore extends FileSystemRequirementsTagProvider{
	
	private static final Logger log = LoggerFactory
			.getLogger(FileSystemRequirementsTagProviderCore.class);
	
	private final String rootDirectoryPath;
    private final NarrativeReader narrativeReader;
    private final int level;

	
	public FileSystemRequirementsTagProviderCore(EnvironmentVariables environmentVariables) {
        this(defaultRootDirectoryPathFrom(Injectors.getInjector().getProvider(EnvironmentVariables.class).get()),
                0,
                environmentVariables);
    }

    public FileSystemRequirementsTagProviderCore(EnvironmentVariables environmentVariables, String root) {
        this(root, 0, environmentVariables);
    }

    public FileSystemRequirementsTagProviderCore() {
        this(defaultRootDirectoryPathFrom(Injectors.getInjector().getProvider(EnvironmentVariables.class).get()));
    }

    public static String defaultRootDirectoryPathFrom(EnvironmentVariables environmentVariables) {
    	log.info("-----------------CUSTOM defaultRootDirectoryPathFrom-----------------------");
        return FileSystemRequirementsTagProvider.defaultRootDirectoryPathFrom(environmentVariables);
    }

    public FileSystemRequirementsTagProviderCore(String rootDirectory, int level) {
        this(filePathFormOf(rootDirectory), level, Injectors.getInjector().getProvider(EnvironmentVariables.class).get());
    }

    /**
     * Convert a package name to a file path if necessary.
     */
    private static String filePathFormOf(String rootDirectory) {
        if (rootDirectory.contains(".")) {
            return rootDirectory.replace(".", "/");
        } else {
            return rootDirectory;
        }
    }

    public FileSystemRequirementsTagProviderCore(String rootDirectory, int level, EnvironmentVariables environmentVariables) {
        super(environmentVariables);
        this.rootDirectoryPath = rootDirectory;
        this.level = level;
        this.narrativeReader = NarrativeReader.forRootDirectory(rootDirectory)
                .withRequirementTypes(getRequirementTypes());
    }

    public FileSystemRequirementsTagProviderCore(String rootDirectory) {
        this(rootDirectory, 0);
    }

}
