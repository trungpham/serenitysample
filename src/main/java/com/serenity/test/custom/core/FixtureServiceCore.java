package com.serenity.test.custom.core;

import net.thucydides.core.fixtureservices.FixtureException;
import net.thucydides.core.fixtureservices.FixtureService;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FixtureServiceCore implements FixtureService{
	private static final Logger LOGGER = LoggerFactory.getLogger(FixtureServiceCore.class);
	
	@Override
	public void setup() throws FixtureException {
		// TODO Auto-generated method stub
		LOGGER.info("----------setup------------");
	}

	@Override
	public void shutdown() throws FixtureException {
		// TODO Auto-generated method stub
		LOGGER.info("----------shutdown------------");
	}

	@Override
	public void addCapabilitiesTo(DesiredCapabilities capabilities) {
		// TODO Auto-generated method stub
		LOGGER.info("----------addCapabilitiesTo------------");
	}

}
