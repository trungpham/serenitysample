package com.serenity.test.custom.core;

import java.util.Map;

import net.thucydides.core.model.DataTable;
import net.thucydides.core.model.Story;
import net.thucydides.core.model.TestOutcome;
import net.thucydides.core.steps.ExecutedStepDescription;
import net.thucydides.core.steps.StepFailure;
import net.thucydides.core.steps.StepListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StepListenerCore implements StepListener{
    private static final Logger LOGGER = LoggerFactory.getLogger(StepListenerCore.class);
    @Override
    public void testSuiteStarted(Class<?> storyClass) {
        // TODO Auto-generated method stub
        LOGGER.info("testSuiteStarted(Class<?> storyClass)");
    }

    @Override
    public void testSuiteStarted(Story story) {
        // TODO Auto-generated method stub
        LOGGER.info("testSuiteStarted(Story story)");
    }

    @Override
    public void testSuiteFinished() {
        // TODO Auto-generated method stub
        LOGGER.info("testSuiteFinished()");
    }

    @Override
    public void testStarted(String description) {
        // TODO Auto-generated method stub
        LOGGER.info("testStarted(String description)");
    }

    @Override
    public void testFinished(TestOutcome result) {
        // TODO Auto-generated method stub
        LOGGER.info("testFinished(TestOutcome result)");
    }

    @Override
    public void testRetried() {
        // TODO Auto-generated method stub
        LOGGER.info("testRetried()");
    }

    @Override
    public void stepStarted(ExecutedStepDescription description) {
        // TODO Auto-generated method stub
        LOGGER.info("stepStarted(ExecutedStepDescription description)");
    }

    @Override
    public void skippedStepStarted(ExecutedStepDescription description) {
        // TODO Auto-generated method stub
        LOGGER.info("skippedStepStarted(ExecutedStepDescription description)");
    }

    @Override
    public void stepFailed(StepFailure failure) {
        // TODO Auto-generated method stub
        LOGGER.info("stepFailed(StepFailure failure)");
    }

    @Override
    public void lastStepFailed(StepFailure failure) {
        // TODO Auto-generated method stub
        LOGGER.info("lastStepFailed(StepFailure failure)");
    }

    @Override
    public void stepIgnored() {
        // TODO Auto-generated method stub
        LOGGER.info("stepIgnored()");
    }

    @Override
    public void stepPending() {
        // TODO Auto-generated method stub
        LOGGER.info("stepPending()");
    }

    @Override
    public void stepPending(String message) {
        // TODO Auto-generated method stub
        LOGGER.info("stepPending(String message)");
    }

    @Override
    public void stepFinished() {
        // TODO Auto-generated method stub
        LOGGER.info("stepFinished()");
    }

    @Override
    public void testFailed(TestOutcome testOutcome, Throwable cause) {
        // TODO Auto-generated method stub
        LOGGER.info("testFailed(TestOutcome testOutcome, Throwable cause)");
    }

    @Override
    public void testIgnored() {
        // TODO Auto-generated method stub
        LOGGER.info("testIgnored()");
    }

    @Override
    public void testSkipped() {
        // TODO Auto-generated method stub
        LOGGER.info("testSkipped()");
    }

    @Override
    public void testPending() {
        // TODO Auto-generated method stub
        LOGGER.info("testPending()");
    }

    @Override
    public void notifyScreenChange() {
        // TODO Auto-generated method stub
        LOGGER.info("notifyScreenChange()");
    }

    @Override
    public void useExamplesFrom(DataTable table) {
        // TODO Auto-generated method stub
        LOGGER.info("useExamplesFrom(DataTable table)");
    }

    @Override
    public void addNewExamplesFrom(DataTable table) {
        // TODO Auto-generated method stub
        LOGGER.info("addNewExamplesFrom(DataTable table)");
    }

    @Override
    public void exampleStarted(Map<String, String> data) {
        // TODO Auto-generated method stub
        LOGGER.info("exampleStarted(Map<String, String> data)");
    }

    @Override
    public void exampleFinished() {
        // TODO Auto-generated method stub
        LOGGER.info("exampleFinished()");
    }

    @Override
    public void assumptionViolated(String message) {
        // TODO Auto-generated method stub
        LOGGER.info("assumptionViolated(String message)");
    }

}
