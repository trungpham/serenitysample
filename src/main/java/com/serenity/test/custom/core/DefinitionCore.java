package com.serenity.test.custom.core;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;

import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.ScenarioType;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sererity.test.pages.DjangoAdminPage;
import com.sererity.test.pages.WikiPage;
import com.sererity.test.steps.DjangoAdminSteps;


public class DefinitionCore{
	
	private static final Logger log = LoggerFactory.getLogger(DefinitionCore.class);
	
	@Steps
	DjangoAdminSteps localStep;

	@Given("Login my system with $username and $password")
	public void loginSystem(String username, String password){
		log.info("Login my system:" + username + "," + password);
		localStep.is_the_home_page();
		localStep.login(username, password);
		localStep.verify_login_success();
	}
	
    @BeforeScenario
    public void beforeEachScenario() {
        log.info("----CALL beforeEachScenario");
        log.info("Feature Meta:" + Serenity.getCurrentSession().getMetaData().get("feature"));
    }
     
    @BeforeScenario(uponType=ScenarioType.EXAMPLE)
    public void beforeEachExampleScenario() {
        log.info("----CALL beforeEachExampleScenario");
    }
         
    @AfterScenario // equivalent to  @AfterScenario(uponOutcome=AfterScenario.Outcome.ANY)
    public void afterAnyScenario() {
        log.info("----CALL afterAnyScenario");
    }
     
    @AfterScenario(uponType=ScenarioType.EXAMPLE)
    public void afterEachExampleScenario() {
        log.info("----CALL afterEachExampleScenario");
    }
         
    @AfterScenario(uponOutcome=AfterScenario.Outcome.SUCCESS)
    public void afterSuccessfulScenario() {
        log.info("----CALL afterSuccessfulScenario");
    }
         
    @AfterScenario(uponOutcome=AfterScenario.Outcome.FAILURE)
    public void afterFailedScenario() {
        log.info("----CALL afterFailedScenario");
    }
}
