# ADVANCED

* **Retrofit need to know (Do with postman first)**

>> Quick create services: https://github.com/Codingpedia/demo-rest-jersey-spring, I downloaded and added more maven plugin, custom mysql connection (demo-rest-jersey-spring-master)

>> Install mysql + mysql workbench, create database rest_demo, (user/pass of mysql are root/root) run script sql from DumpRESTdemoDB.sql file for create table and init sample data

>> Go to demo-rest-jersey-spring-master, run maven: mvn clean install jetty:run -Djetty.port=8888

>> Open url: http://localhost:8888/demo-rest-jersey-spring/podcasts ==> Get all postcast

>> http://localhost:8888/demo-rest-jersey-spring/podcasts/1 with Method GET ==> Get Postcast with id 1

>> http://localhost:8888/demo-rest-jersey-spring/podcasts/1 with Method DELETE ==> Get Postcast with id 1

>> Create postcast:

>>>> Fill link: http://localhost:8888/demo-rest-jersey-spring/podcasts

>>>> Method: POST (not GET)

>>>> Choose Raw instead of form-data

>>>> Fill json object for passing postcast object (see json example)

>>>> Press send button

>>>> json example:

>>>> {
  "title": "Trung Test 3",
  "linkOnPodcastpedia": "http://www.podcastpedia.org/podcasts/1/Quarks-Co-zum-Mitnehmen3",
  "feed": "http://podcast.wdr.de/quarks.xml3",
  "description": "Quarks & Co: Das Wissenschaftsmagazin3"
}


>> Update postcast (Similar Create action):

>>>> Another json example (similar) with id

>>>> Link: http://localhost:8888/demo-rest-jersey-spring/podcasts/{id}

>>>> Call with method PUT


* **Guice (DI lib)**
 
>> Most important (When hook anything in Serenity system, we need more understand about it. Because expansible of Serenity is designed base on Guice and below Services dynamic - I read net.serenitybdd.plugins.jira.guice.Injectors for knowing that)

>> https://github.com/google/guice/wiki/GettingStarted, you just read and write example User's Guice.

>> When you got basic knowledge, read net.serenitybdd.plugins.jira.guice.Injectors and net.serenitybdd.plugins.jira.guice.ThucydidesJiraModule classes for knowing more about how to Serenity work.

>> Write example: Use it for creating your retrofit library.

* **lambdaj** - List utils useful (http://code.google.com/p/lambdaj/), read for improving code skill.

* **Retrofit**

>>>> Retrofit mock and example: http://touk.pl/blog/en/2014/02/26/mock-retrofit-using-dagger-and-mockito/

>>>> http://doridori.github.io/Retrofit,%20MockWebServer%20&%20Dagger/

>>>> http://newbility.blogspot.com/2014/02/mock-retrofit-using-dagger.html

* **Services dynamic (DI lib)**

>> It's important, like Guice. Read ClasspathFixtureProviderService for knowing how to use ServiceLoader.

>> StepListener

>>>> Hook to Step (run and read more log StepListenerCore.java)

>>>> You can do something before, or after or some others action. Example: You can create your style report seperate with Serenity Report or comment log to your server... (Jira plugin use this structure for comment, update status...) 

>> TagProvider

>>>> Hook to TagProvider (Scan folder for creating structure serenity report types)

>>>> You can restructure requirement types (Now we just read for knowing) 

>> AcceptanceTestReporter

>>>> Hook (May be change or some minor edit for Report)

>> FixtureService

>>>> setup/shutdown/addCapabilitiesTo (May be use for proxing), Try but not work

>> ContextConfiguration (default it use SpringDependencyInjector if dependency has serenity-spring package, read configuration from @ContextConfiguration), you can use Guice for replacement (I think Guice better) 

>>>> DatabaseConfig.java (common interface)

>>>> HsqlDatabaseConfig.java, MySqlDatabaseConfig.java (implement class)

>>>> DatabaseConfigurationAnnotation.java (Configuration annotation file, or can use config file as database-config.xml) ==> Target of two file is init bean and inject to every test case

>>>> Example runner: ApiOnline.java (read @ContextConfiguration, @Autowired, logDatabaseConfigInfo) 

>> Best example: JiraListener file.

>> WebDriverFacade.java, WebDriverFactory.java, WebdriverInstanceFactory, DriverCapabilityRecord related with Fixture, need read more

#Meta
* Not working with -, working with underscore ( _ )
* Syntax: @feature Reporting instead of @tag feature: Reporing
* Know lifecycle (http://jbehave.org/reference/stable/story-syntax.html)
* Use @Ignore meta annotation in story file for ignore test

------------
#More about Selenium + JBehave + Serenity

* **Custom keyword** 

>> (Add Having same as And keyword). File i18n/keywords_en.properties for English. If you want change to another file, call useKeywords method in instance of Configuration class.

>> http://jbehave.org/reference/stable/keyword-synonyms.html

* **Tag Provider**

>> META-INF/services/net.thucydides.core.statistics.service.TagProvider

>> FileSystemRequirementsTagProviderCore, custom success but duplicate when extends FileSystemRequirementsTagProvider (exists provider), only implements RequirementsTagProvider interface, I commented by # character before

>> META-INF/services/net.thucydides.core.reports.AcceptanceTestReporter


* **Check mainframe available**

>> Use org.junit.Assume.assumeThat for check mainframe available, if not available ==> Ignore the rest test case.

>> When use it? Ex: Before test web or anything, we need sure system already for test.

* **Annotation**

>> Read and test some annotation when it work!

>> Ex: @BeforeScenario only work in Definition class, not working in other type class (Step, Page...)

>> Read DefinitionCore class, and subclass extends it. Run and see log for know how it work.

>> Use @Alias and @Aliases

>> @ManagedPages ==> Inject all pages to variable ==> getPages().get(WikiPage.class) instead declare WikiPage wikiPage

>>>> http://thucydides.info/docs/serenity-staging/#_using_fluent_matcher_expressions

>> http://jbehave.org/reference/stable/annotations.html


* **Parameter Converters**

>> http://jbehave.org/reference/stable/parameter-converters.html

>> Add sample PersonConverter and custom datetime format from story file (Read more AcceptanceTest.java, method configuration for more detail)

>> Use PersonConverterAnnotation (Simpler, but is it good when reuse all step?)

* **serenity.properties**

>> http://thucydides.info/docs/serenity-staging/

>> Read and note useful properties

>> Read more and careful about integrate Jira

>>>> http://thucydides-webtests.com/2014/07/10/bdd-requirements-management-with-jbehave-thucydides-and-jira-part-1/

>>>> http://thucydides-webtests.com/2014/07/14/bdd-requirements-management-with-jbehave-thucydides-and-jira-part-2/

>>>> These pages are written long time ago, but they still useful. (Try replace all thucydides properties with serenity in these guide) ==> Note some somefeature.

>>>> Filled information serenity.properties. Worked! But need more. (API newest not support comment on Jira, older version support)

>>>> http://thucydides.info/docs/target/thucydides.html

>> serenity.requirements.dir: Tested - Change this Report will restructure from src/test/resources/script instead of src/test/resources/stories and src/test/resources/features




* **Custom PageObject**

>> Do action after open url (use @WhenPageOpens), ex: Open maximum browser window when we open any link.

>> Add more option for driver (override getDriver() method)

>> ...

* **Loading parameters from an external resource**

>> http://jbehave.org/reference/stable/parametrised-scenarios.html

>> Not important (Read later)

* **Composite Steps**

>> Useful: Mixin steps, write 1 demo

>> http://jbehave.org/reference/stable/composite-steps.html

* **Cross Reference**

>> Special report: target/jbehave/view/xref.json

>> http://jbehave.org/reference/stable/cross-reference.html

* **Tabular Parameters**

>> Table like Example given
	
>> http://jbehave.org/reference/stable/tabular-parameters.html

* **Meta Parametrisation: Read meta in your method**

>> http://jbehave.org/reference/stable/meta-parametrisation.html

* **Don't comment Scenario**

>> Error in before scenario (can't read !-- character)

* **WebElementFacade**

>> Add more verify element stage before do action ==> List some verify useful

>> http://thucydides-webtests.com/2015/03/20/serenity-2-0-42-and-the-new-timeout-api/

* **Read more outcomes table**

* **Remoting Browser test**

>> Start hub -> Open node connect to hub: http://www.seleniumhq.org/docs/07_selenium_grid.jsp

>>>> Run java -jar <selenium-server-standalone-jar-package> -hub

>>>> Run Serenity with param -Dwebdriver.remote.url=http://127.0.0.1:4444/wd/hub

* **Get Current session info**

>> Use Map<String, String> metadata = Serenity.getCurrentSession().getMetaData();
inside @Step function for get metadata

* Use @Managed(uniqueSession) ==> Not important
* SerenityParameterizedRunner (data list)
* Use with @UseTestDataFrom(value=".csv", separator=';') for read from CSV
* Concurrent (Run tests in parallel) threads="4"

* **Description HTML Test case (Not important)**

>> http://thucydides-webtests.com/2013/02/21/thucydides-release-0-9-98-better-screenshots-management/

* **Easyb**

>> Similar Jbehave, Cucumber, story syntax is json instead normal text

>> http://thucydides-webtests.com/2012/05/15/thucydides-release-0-8-12-adding-tag-support-to-easyb-stories/


* **Faster Web Tests with Parallel Batches in Thucydides**

>> http://thucydides-webtests.com/2011/12/25/faster-web-tests-with-parallel-batches-in-thucydides/


* **Rest API Test**

>> **rest-assured**

>>>> http://code.google.com/p/rest-assured/wiki/GettingStarted

>> **Retrofit**

>>>> http://square.github.io/retrofit/

>> **Service sample**
	
>>>> http://jsonplaceholder.typicode.com/
	
------------
	
#Some MAVEN Info

* **Read more about Maven Lifecycle**

>> https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html

* **Use custom property depends enviroment**

>> Use filter, resources, profiles

* Plugin maven-failsafe-plugin will scan directories in configuration (includes property)

>> post-integration-test ==> run integration-test will call post-integration-test

* mvn verify or mvn integration-test for run and create report files (target folder)

* serenity integrate webdriver, change version by change webdriver.driver property in maven

* slf4j-simple (ver >= 1.7.5 for simplelogger.properties working correctly - turn on DEBUG...)

* slf4j-log4j12 (ver >= 1.7.5 better slf4j-simple with log4j.properties, readmore: http://www.slf4j.org/nlog4j/api/org/apache/log4j/PatternLayout.html)

* OVERRIDE LOG LEVEL FOR PACKAGE (apply for class) log4j.properties
* log4j.logger.<package> = <level_log>
* https://github.com/serenity-bdd/serenity-demos

* **Default property**

>> http://www.captaindebug.com/2011/01/maven-default-properties.html#.VYpVZHWUdCU

* **maven-failsafe-plugin**

>> <maven.test.failure.ignore>true</maven.test.failure.ignore> for force create report even when test case fail.

>> Read more option http://maven.apache.org/surefire/maven-surefire-plugin/test-mojo.html

* **Remote DEBUGGING (Tested)**

>> Start server: http://maven.apache.org/surefire/maven-surefire-plugin/examples/debugging.html

>> Listener (Choose Socket Attach in Eclipse): http://help.eclipse.org/luna/index.jsp?topic=%2Forg.eclipse.jdt.doc.user%2Ftasks%2Ftask-remotejava_launch_config.htm


# JENKINS

>> Download jenkins.war file

>> https://wiki.jenkins-ci.org/display/JENKINS/Starting+and+Accessing+Jenkins

* Start Server: java -jar jenkins.war

* Go to http://localhost:8000 (Default)

* Enable security (If need)

* Install Git Plugin (Manage => http://localhost:8080/pluginManager/ ==> available tab)

* Setup Maven + JDK

* Create Project Sample (configuration with git)

* Build for test (Read info in build for know workspace of Jenkins and git)  

* Test send mail